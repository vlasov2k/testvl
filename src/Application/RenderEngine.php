<?php
declare(strict_types=1);

namespace App\Application;

use Twig_Environment;
use Twig_Loader_Filesystem;

class RenderEngine
{
    private Twig_Environment $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../../Templates/');
        $this->twig = new Twig_Environment($loader);
    }

    public function render($content): string
    {
        return $this->twig->render(
            'main.html',
            [
                'categories' => $content
            ]
        );
    }
}
