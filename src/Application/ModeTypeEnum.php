<?php
declare(strict_types=1);

namespace App\Application;

class ModeTypeEnum
{
    public const CLI = 1;
    public const WEB = 2;
}
