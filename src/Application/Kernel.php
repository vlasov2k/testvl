<?php
declare(strict_types=1);

namespace App\Application;

use App\Application\Command\CommandFactory;
use App\Application\Controllers\CategoryListController;

class Kernel
{
    public function run(int $modeType): void
    {
        if ($modeType === ModeTypeEnum::CLI) {
            $this->executeCLICommands();
        }

        if ($modeType === ModeTypeEnum::WEB) {
            $this->handleWebRequest();
        }
    }

    private function executeCLICommands(): void
    {
        $commandFactory = new CommandFactory();
        $commandFactory->createImportCommand()->execute();
        $commandFactory->createExportCommand()->execute();
    }

    private function handleWebRequest(): void
    {
        echo (new CategoryListController())->getResponse();
    }
}
