<?php
declare(strict_types=1);

namespace App\Application\Dictionary;

class OutputMessageTemplates
{
    private const MESSAGE = [
        'importFilePath' => 'type file path or press [enter] to default (categories.json): ',
        'viewType' => 'select the nesting level to export data to a file: ' . "\n" .
            'full list press [enter], category data no further than the first level of nesting type [2]: ',
        'exportFilePath' => 'Type the name of the file in which to write data or press [enter] to default: ',
        'enteredFilePath' => 'The "%s" is used',
        'notRequiredExtension' => 'WARNING: wrong extension "%s"! "%s" required!'
    ];

    public function getImportFilePath(): string
    {
        return self::MESSAGE['importFilePath'];
    }

    public function getViewType(): string
    {
        return self::MESSAGE['viewType'];
    }

    public function getExportFilePath(): string
    {
        return self::MESSAGE['exportFilePath'];
    }

    public function onEnteredFilePath(string $filePath): string
    {
        return sprintf(
            self::MESSAGE['enteredFilePath'],
            $filePath
        );
    }

    public function onPassNotRequiredExtension(
        string $currentExtensionType,
        string $requiredExtensionType
    ): string {
        return sprintf(
        self::MESSAGE['notRequiredExtension'],
            $currentExtensionType,
            $requiredExtensionType
        );
    }
}
