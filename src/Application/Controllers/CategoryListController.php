<?php
declare(strict_types=1);

namespace App\Application\Controllers;

use App\Domain\Categories\Entity\CategoryRepository;
use App\Domain\Categories\ViewStrategies\ViewStrategy;
use App\Application\RenderEngine;

class CategoryListController
{
    private CategoryRepository $categoryRepository;
    private RenderEngine $renderEngine;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->renderEngine = new RenderEngine();
    }

    public function getResponse(): string
    {
        $categoryCollection = $this->categoryRepository->getCollection();
        $viewStrategy = new ViewStrategy();
        $categories = $categoryCollection->collectionToView($viewStrategy);

        return $this->renderEngine->render($categories);
    }
}
