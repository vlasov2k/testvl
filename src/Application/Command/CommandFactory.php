<?php
declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Categories\Entity\CategoryRepository;
use App\Infrastructure\Transport\ConsoleTransport;
use App\Application\Dictionary\OutputMessageTemplates;

class CommandFactory
{
    private CategoryRepository $categoryRepository;
    private OutputMessageTemplates $outputMessageTemplates;
    private ConsoleTransport $consoleTransport;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->outputMessageTemplates = new OutputMessageTemplates();
        $this->consoleTransport = new ConsoleTransport();
    }

    public function createImportCommand(): ImportCommand
    {
        return new ImportCommand(
            $this->categoryRepository,
            $this->outputMessageTemplates,
            $this->consoleTransport
        );
    }

    public function createExportCommand(): ExportCommand
    {
        return new ExportCommand(
            $this->categoryRepository,
            $this->outputMessageTemplates,
            $this->consoleTransport
        );
    }
}