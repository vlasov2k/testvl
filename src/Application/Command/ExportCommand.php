<?php
declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Categories\Entity\CategoryRepository;
use App\Domain\Categories\ViewStrategies\ViewStrategy;
use App\Application\Dictionary\OutputMessageTemplates;
use App\Infrastructure\Transport\ConsoleTransport;
use App\Infrastructure\Transport\FileTransport;
use App\Infrastructure\Transport\FileTransportException;

class ExportCommand
{
    private CategoryRepository $categoryRepository;
    private OutputMessageTemplates $outputMessageTemplates;
    private ConsoleTransport $consoleTransport;

    private const DEFAULT_FILE_A = 'type_a.txt';
    private const DEFAULT_FILE_B = 'type_b.txt';
    private const SHORT_VIEW_TYPE = 2;
    private const LONG_VIEW_TYPE = 0;

    private int $viewType;
    private string $filePath;

    public function __construct(
        CategoryRepository $categoryRepository,
        OutputMessageTemplates $outputMessageTemplates,
        ConsoleTransport $consoleTransport
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->outputMessageTemplates = $outputMessageTemplates;
        $this->consoleTransport = $consoleTransport;
    }

    /**
     * последовательное выполнение экспорта данных в файл
     */
    public function execute(): void
    {
        try {
            $this->getViewType();
            $this->getFilePath();
            $this->export();
        } catch (FileTransportException $e) {
            $this->consoleTransport->send($e->getMessage());
            $this->execute();
        }
    }

    private function getViewType(): void
    {
        $console = $this->consoleTransport;
        $message = $this->outputMessageTemplates;

        $console->send($message->getViewType());
        $this->viewType = $console->getInt();

        if ($this->viewType !== self::LONG_VIEW_TYPE && $this->viewType !== self::SHORT_VIEW_TYPE) {
            $this->getViewType();
        }
    }

    private function getFilePath(): void
    {
        $console = $this->consoleTransport;
        $message = $this->outputMessageTemplates;

        $console->send($message->getExportFilePath());
        $this->filePath = $console->getString();
        if ($this->filePath === '') {
            if ($this->viewType === self::LONG_VIEW_TYPE) {
                $this->filePath = self::DEFAULT_FILE_A;
            }
            if ($this->viewType === self::SHORT_VIEW_TYPE) {
                $this->filePath = self::DEFAULT_FILE_B;
            }
        }
        $console->send($message->onEnteredFilePath($this->filePath));
    }

    /**
     * @throws FileTransportException
     */
    private function export(): void
    {
        $file = new FileTransport($this->filePath, false);
        $strategy = new ViewStrategy($this->viewType);

        $categoryCollection = $this->categoryRepository->getCollection();
        $collectionToView = $categoryCollection->collectionToView($strategy);
        $file->writeFile(\implode("\n", $collectionToView));
    }
}
