<?php
declare(strict_types=1);

namespace App\Application\Command;


use App\Domain\Categories\Builder\CategoryCollectionBuilderException;
use App\Domain\Categories\Entity\CategoryRepository;
use App\Application\Dictionary\OutputMessageTemplates;
use App\Infrastructure\Transport\ConsoleTransport;
use App\Infrastructure\Transport\DatabaseTransportException;
use App\Infrastructure\Transport\FileTransport;
use App\Infrastructure\Transport\FileTransportException;

class ImportCommand
{
    private const REQUIRED_EXTENSION = '.json';
    private const DEFAULT_FILE = 'categories' . self::REQUIRED_EXTENSION;

    private string $filePath;

    private CategoryRepository $categoryRepository;
    private OutputMessageTemplates $outputMessageTemplates;
    private ConsoleTransport $consoleTransport;

    private FileTransport $fileTransport;

    public function __construct(
        CategoryRepository $categoryRepository,
        OutputMessageTemplates $outputMessageTemplates,
        ConsoleTransport $consoleTransport
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->outputMessageTemplates = $outputMessageTemplates;
        $this->consoleTransport = $consoleTransport;
    }

    /**
     * последовательное выполнение импорта файла в базу данных
     */
    public function execute(): void
    {
        try {
            $this->getFilePath();

            $this->fileTransport = new FileTransport($this->filePath);
            if (!$this->extensionAllowed()) {
                $this->execute();
            }

            $this->import();
        } catch (FileTransportException | CategoryCollectionBuilderException | DatabaseTransportException $e) {
            $this->consoleTransport->send($e->getMessage());
            $this->execute();
        }
    }

    private function getFilePath(): void
    {
        $console = $this->consoleTransport;
        $message = $this->outputMessageTemplates;

        $console->send($message->getImportFilePath());
        $this->filePath = $console->getString();
        if ($this->filePath === '') {
            $this->filePath = self::DEFAULT_FILE;
        }
        $console->send($message->onEnteredFilePath($this->filePath)
        );
    }

    private function extensionAllowed(): bool
    {
        $console = $this->consoleTransport;
        $message = $this->outputMessageTemplates;
        $file = $this->fileTransport;

        $isJson = $file->getExtensionType() === self::REQUIRED_EXTENSION;
        if (!$isJson) {
            $console->send(
                $message->onPassNotRequiredExtension(
                    $file->getExtensionType(),
                    self::REQUIRED_EXTENSION
                )
            );

            return false;
        }

        return true;
    }

    /**
     * @throws CategoryCollectionBuilderException
     * @throws DatabaseTransportException
     * @throws FileTransportException
     */
    private function import(): void
    {
        $categories = $this->fileTransport->readFromFile();
        $this->categoryRepository->save($categories);
    }
}
