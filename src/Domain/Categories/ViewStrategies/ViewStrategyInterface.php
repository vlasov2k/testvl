<?php
declare(strict_types=1);

namespace App\Domain\Categories\ViewStrategies;

use App\Domain\Categories\Entity\Category;

interface ViewStrategyInterface
{
    public function transform(Category $category): array;
}
