<?php
declare(strict_types=1);

namespace App\Domain\Categories\ViewStrategies;

use App\Domain\Categories\Entity\Category;

class ViewStrategy implements ViewStrategyInterface
{
    private int $nestedLevelLimit;
    private array $result;

    public function __construct(int $nestedLevelLimit = 0)
    {
        $this->nestedLevelLimit = $nestedLevelLimit;
    }

    public function transform(Category $category): array
    {
        $includeChildren = $this->nestedLevelLimit === 0 || $category->getDepthLevel() < $this->nestedLevelLimit;
        $indent = str_repeat("\t", $category->getDepthLevel() - 1);
        $this->result[] = $indent . $category->getName() . ' ' . $category->getUrl();
        if ($includeChildren && $category->hasChild()) {
            $category->getChildren()->collectionToView($this);
        }

        return $this->result;
    }
}
