<?php
declare(strict_types=1);

namespace App\Domain\Categories\Entity;

use App\Domain\Categories\ViewStrategies\ViewStrategyInterface;
use function substr_count;

class Category
{
    private int $id;
    private int $parentId;
    private string $url;
    private string $name;
    private string $alias;
    private CategoryCollection $children;

    public function __construct(
        int $id,
        int $parentId,
        string $url,
        string $name,
        string $alias,
        CategoryCollection $children
    )
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->url = $url;
        $this->name = $name;
        $this->alias = $alias;
        $this->children = $children;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getDepthLevel(): int
    {
        return substr_count($this->getUrl(), '/');
    }

    public function getChildren(): CategoryCollection
    {
        return $this->children;
    }

    public function hasChild(): bool
    {
        return !empty($this->getChildren()->getCollection());
    }

    public function categoryToView(ViewStrategyInterface $strategy): array
    {
        return $strategy->transform($this);
    }
}
