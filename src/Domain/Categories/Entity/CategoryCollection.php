<?php
declare(strict_types=1);

namespace App\Domain\Categories\Entity;

use App\Domain\Categories\ViewStrategies\ViewStrategyInterface;

class CategoryCollection
{
    /**
     * @var Category[]
     */
    private array $collection = [];

    /**
     * @return Category[]
     */
    public function getCollection(): array
    {
        return $this->collection;
    }

    public function addCategory(Category $category): void
    {
        $this->collection[] = $category;
    }

    public function collectionToView(ViewStrategyInterface $strategy): array
    {
        $result = [];
        foreach ($this->getCollection() as $item) {
            $result += $item->categoryToView($strategy);
        }

        return $result;
    }
}
