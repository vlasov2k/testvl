<?php
declare(strict_types=1);

namespace App\Domain\Categories\Entity;

use App\Domain\Categories\Builder\CategoryCollectionBuilder;
use App\Domain\Categories\Builder\CategoryCollectionBuilderException;
use App\Infrastructure\Transport\DatabaseTransport;
use App\Infrastructure\Transport\DatabaseTransportException;

class CategoryRepository
{
    private DatabaseTransport $databaseTransport;
    private CategoryCollectionBuilder $categoryCollectionBuilder;

    public function __construct()
    {
        $this->databaseTransport = new DatabaseTransport();
        $this->categoryCollectionBuilder = new CategoryCollectionBuilder();
    }

    /**
     * @param $content
     *
     * @throws DatabaseTransportException
     * @throws CategoryCollectionBuilderException
     */
    public function save($content): void
    {
        $this->tableInit();

        $categoryCollection = $this->categoryCollectionBuilder->buildFromJson($content);
        $this->saveCollection($categoryCollection);
    }

    public function getCollection(): CategoryCollection
    {
        $dbData = $this->databaseTransport->select();

        return $this->categoryCollectionBuilder->buildFromDB($dbData);
    }


    private function tableInit(): void
    {
        if ($this->databaseTransport->tableExists()) {
            $this->databaseTransport->truncateTable();
        } else {
            $this->databaseTransport->createTable();
        }
    }

    /**
     * @param CategoryCollection $categoryCollection
     *
     * @throws DatabaseTransportException
     */
    private function saveCollection(CategoryCollection $categoryCollection): void
    {
        $collection = $categoryCollection->getCollection();
        foreach ($collection as $category) {
            $this->saveCategory($category);
            if ($category->hasChild()) {
                $this->saveCollection($category->getChildren());
            }
        }
    }

    /**
     * @param Category $category
     *
     * @throws DatabaseTransportException
     */
    private function saveCategory(Category $category): void
    {
        $this->databaseTransport->insert(
            $category->getId(),
            $category->getParentId(),
            $category->getUrl(),
            $category->getName(),
            $category->getAlias()
        );
    }
}
