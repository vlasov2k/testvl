<?php
declare(strict_types=1);

namespace App\Domain\Categories\Builder;

use App\Domain\Categories\Entity\Category;
use App\Domain\Categories\Entity\CategoryCollection;

class CategoryCollectionBuilder
{
    /**
     * @param string $content
     *
     * @return CategoryCollection
     *
     * @throws CategoryCollectionBuilderException
     */
    public function buildFromJson(string $content): CategoryCollection
    {
        $nestedArray = self::createNestedArrayFromJson($content);

        return self::createCategoryCollectionFromNestedArrayByChildren($nestedArray);
    }

    public function buildFromDB(array $collection): CategoryCollection
    {
        $collection = self::setArrayKeys($collection);
        $collection = self::createNestedArray($collection);

        return self::createCategoryCollectionFromNestedArrayByParentId($collection);
    }

    /**
     * @param string $content
     *
     * @return array
     *
     * @throws CategoryCollectionBuilderException
     */
    private static function createNestedArrayFromJson(string $content): array
    {
        try {
            return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw new CategoryCollectionBuilderException('WARNING: can\'t decode file');
        }
    }

    private static function createCategoryCollectionFromNestedArrayByChildren(
        array $collection,
        int $parentId = 0,
        string $url = ''
    ): CategoryCollection {
        $resultCollection = new CategoryCollection();

        foreach ($collection as $item) {
            $itemUrl = $url . '/' . $item['alias'];
            $resultCollection->addCategory(
                new Category(
                    $item['id'],
                    $parentId,
                    $itemUrl,
                    $item['name'],
                    $item['alias'],
                    (!empty(array_keys($item)[3]))
                        ? self::createCategoryCollectionFromNestedArrayByChildren(
                        $item[array_keys($item)[3]],
                        $item['id'],
                        $itemUrl
                    )
                        : new CategoryCollection()
                )
            );
        }

        return $resultCollection;
    }

    private static function setArrayKeys($importArray): array
    {
        $filteredKeysArray = [];
        foreach ($importArray as $key => $value) {
            $filteredKeysArray[$value['id']] = $value;
        }

        return $filteredKeysArray;
    }

    private static function createNestedArray(array $collection): array
    {
        $resultCollection = [];
        foreach ($collection as $key => &$value) {
            if ($value['parent_id']) {
                $collection[$value['parent_id']]['childrens'][$key] = &$value;
            } else {
                $resultCollection[$key] = &$value;
            }
        }

        return $resultCollection;
    }

    private static function createCategoryCollectionFromNestedArrayByParentId(array $collection): CategoryCollection
    {
        $resultCollection = new CategoryCollection();
        foreach ($collection as $key => $item) {
            $resultCollection->addCategory(
                new Category(
                    (int)$item['id'],
                    (int)$item['parent_id'],
                    $item['url'],
                    $item['name'],
                    $item['alias'],
                    (!empty($item['childrens']))
                        ? self::createCategoryCollectionFromNestedArrayByParentId($item['childrens'])
                        : new CategoryCollection()
                )
            );
        }

        return $resultCollection;
    }
}
