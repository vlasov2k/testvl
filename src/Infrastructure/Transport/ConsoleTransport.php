<?php
declare(strict_types=1);

namespace App\Infrastructure\Transport;

class ConsoleTransport
{
    public function getString(): string
    {
        return trim(
            strip_tags(
                str_replace(
                    ' ',
                    '',
                    readline())))
            ?: '';
    }

    public function getInt(): int
    {
        return abs((int)readline() ?: 0);
    }

    public function send(string $message): void
    {
        echo "\n{$message}";
    }
}
