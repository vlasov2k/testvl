<?php
declare (strict_types=1);

namespace App\Infrastructure\Transport;

use PDO;
use PDOException;

class DatabaseTransport
{
    private PDO $pdo;

    /**
     * @var array|string[]
     */
    private array $query = [
        'tableExists' => 'SHOW TABLES LIKE \'' . DEFAULT_TABLE_NAME . '\'',
        'createTable' => 'CREATE TABLE IF NOT EXISTS '. DEFAULT_TABLE_NAME . ' (
                                id INT NOT NULL PRIMARY KEY NOT NULL,
                                parent_id INT,
                                url VARCHAR (100) NOT NULL,
                                name VARCHAR (100) NOT NULL,
                                alias VARCHAR (100) NOT NULL)',
        'truncate' => 'TRUNCATE ' . DEFAULT_TABLE_NAME,
        'insertIntoTable' => 'INSERT INTO ' . DEFAULT_TABLE_NAME . ' (
                                id, 
                                parent_id, 
                                url, 
                                name, 
                                alias
                            ) VALUES (
                                :id,
                                :parent_id,
                                :url,
                                :name,
                                :alias 
                            )',
        'select' => 'SELECT  
                            id,
                            parent_id, 
                            url, 
                            name, 
                            alias 
                    FROM ' . DEFAULT_TABLE_NAME . ' 
                    ORDER BY id'
    ];

    /**
     * DatabaseTransport constructor.
     *
     * @throws DatabaseTransportException
     */
    public function __construct()
    {
        $this->connect();
    }

    public function tableExists(): bool
    {
        return \count(($this->pdo->query($this->query['tableExists']))->fetchAll()) === 1;
    }

    public function createTable(): void
    {
        $this->pdo->exec($this->query['createTable']);
    }

    public function truncateTable(): void
    {
        $this->pdo->exec($this->query['truncate']);
    }

    /**
     * @param int $id
     * @param int $parent_id
     * @param string $url
     * @param string $name
     * @param string $alias
     *
     * @return bool
     *
     * @throws DatabaseTransportException
     */
    public function insert(
        int $id,
        int $parent_id,
        string $url,
        string $name,
        string $alias
    ): bool {
        $statement = $this->pdo->prepare($this->query['insertIntoTable']);

        try {
            return $statement->execute([
                ':id' => $id,
                ':parent_id' => $parent_id,
                ':url' => $url,
                ':name' => $name,
                ':alias' => $alias
            ]);
        } catch (PDOException $e) {
            throw new DatabaseTransportException('can\'t save data');
        }
    }

    public function select(): array
    {
        return ($this->pdo->query($this->query['select']))->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return void
     *
     * @throws DatabaseTransportException
     */
    private function connect(): void
    {
        try {
            $this->pdo = new PDO (DSN, USER, PASSWORD);
        } catch (PDOException $e) {
            throw new DatabaseTransportException('can\'t connect to database');
        }
    }
}
