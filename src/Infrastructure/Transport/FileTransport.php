<?php
declare(strict_types=1);

namespace App\Infrastructure\Transport;

class FileTransport
{
    private string $filePath;
    private bool $isReadOnly;

    /**
     * FileTransport constructor.
     * @param string $filePath
     * @param bool $isReadOnly
     *
     * @throws  FileTransportException
     */
    public function __construct(string $filePath, bool $isReadOnly = true)
    {
        $this->filePath = $filePath;
        $this->isReadOnly = $isReadOnly;
        $this->validateFile();
    }

    /**
     * @throws FileTransportException
     */
    public function validateFile(): void
    {
        $isReadOnly = $this->isReadOnly;
        $filePath = $this->filePath;
        if ($isReadOnly && !file_exists($filePath)) {
            throw new FileTransportException('WARNING: file doesn\'t exists');
        }
        if (!$isReadOnly && file_exists($filePath)) {
            throw new FileTransportException('WARNING: file already exists');
        }
    }

    /**
     * @return string
     * @throws FileTransportException
     */
    public function readFromFile(): string
    {
        $filePath = $this->filePath;
        $content = file_get_contents($filePath);

        if (!$content) {
            throw new FileTransportException('WARNING: can\'t read file');
        }
        return $content;
    }

    public function getExtensionType(): string
    {
        $extensions = [];
        preg_match(
            '/\.(.*)$/',
            $this->filePath,
            $extensions
        );
        return $extensions[0] ?? '';
    }

    /**
     * @param $content
     * @return bool
     * @throws FileTransportException
     */
    public function writeFile($content): bool
    {
        if (!file_put_contents($this->filePath, $content)) {
            throw new FileTransportException('WARNING: can\'t write file');
        }
        return true;
    }
}
