#create database and user
CREATE DATABASE vladlink CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI;
CREATE USER vladlink;
USE vladlink;
GRANT CREATE, INSERT, SELECT, DELETE, DROP ON * TO vladlink;
FLUSH PRIVILEGES;

#drop database and user
DROP DATABASE vladlink;
DROP USER vladlink;

