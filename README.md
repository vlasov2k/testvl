####Functionality:

    - reads a list of multidimensional categories menu (hereinafter data) from a json file.
    - saves data in a database.
    - saves data from the database to a file.
    
Data is saved to a file with a choice of nesting:
Full list, list no further than the first level of nesting.

####Requirements:
For the application to work you need:

    - COMPOSER
    - DATABASE SERVER
        - database and user (to create see dump.sql file)

#####required file format:

        {
        "id": 3,
        "name": "\u0421\u043f\u0438\u0441\u043e\u043a",
        "alias": "list"[,
        "childrens": [
          {
            "id": 4,
            "name": "\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0435",
            "alias": "active"
          }] ]
        }
        
To start the application in the project directory enter index.php
To exit the application press ctrl + enter.