<?php
declare (strict_types=1);

const HOST = 'localhost';
const DB_NAME = 'vladlink';
const DSN = 'mysql:host=' . HOST . ';dbname=' . DB_NAME;
const USER = 'vladlink';
const PASSWORD = '';
const DEFAULT_TABLE_NAME = 'categories';
