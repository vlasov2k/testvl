<?php
declare(strict_types=1);

require_once __DIR__ . '/config/config.php';
require_once __DIR__ . '/vendor/autoload.php';

use App\Application\Kernel;

$kernel = new Kernel();
$modeType = getModeType();

$kernel->run($modeType);

function getModeType(): int
{
    if (!isset($_SERVER['REMOTE_ADDR'])) {
        return \App\Application\ModeTypeEnum::CLI;
    }

    return \App\Application\ModeTypeEnum::WEB;
}
